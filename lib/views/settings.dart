import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:gallery_ku_project/widgets/widget.dart';

class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: BrandName(),
        elevation: 0.0,
      ),
      body: Center(
        child: Text(
          "Not ready",
          style: TextStyle(
              fontSize: 50,
              color: Colors.orange,
              fontWeight: FontWeight.bold,
              fontFeatures: [FontFeature.enable('smcp')]),
        ),
      ),
    );
  }
}
