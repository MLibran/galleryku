import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:gallery_ku_project/model/wallpaper_model.dart';
import 'package:gallery_ku_project/views/image_view.dart';

Widget BrandName() {
  return RichText(
    text: TextSpan(
      style:
          TextStyle(fontSize: 20, fontFeatures: [FontFeature.enable('smcp')]),
      children: <TextSpan>[
        TextSpan(
            text: 'Gallery',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.black87)),
        TextSpan(
            text: 'Ku',
            style:
                TextStyle(fontWeight: FontWeight.bold, color: Colors.orange)),
      ],
    ),
  );
}

Widget wallpapersList({List<WallpaperModel> wallpapers, context}) {
  return Container(
    padding: EdgeInsets.symmetric(horizontal: 16),
    child: GridView.count(
      shrinkWrap: true,
      physics: ClampingScrollPhysics(),
      crossAxisCount: 2,
      childAspectRatio: 0.6,
      mainAxisSpacing: 6.0,
      crossAxisSpacing: 6.0,
      children: wallpapers.map((wallpaper) {
        return GridTile(
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => ImageView(
                      imgUrl: wallpaper.src.portrait,
                    ),
                  ));
            },
            child: Hero(
              tag: wallpaper.src.portrait,
              child: Container(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(16),
                  child: Image.network(
                    wallpaper.src.portrait,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
            ),
          ),
        );
      }).toList(),
    ),
  );
}
